using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace lagoclient;

public partial class LagoClient
{
    partial void UpdateJsonSerializerSettings(JsonSerializerSettings settings) {
        settings.ContractResolver = new SafeContractResolver();
    }

    class SafeContractResolver : DefaultContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization) {
            var jsonProp = base.CreateProperty(member, memberSerialization);
            jsonProp.Required = Required.Default;   // This is necessary to ignore null and absent properties
            return jsonProp;
        }
    }
}