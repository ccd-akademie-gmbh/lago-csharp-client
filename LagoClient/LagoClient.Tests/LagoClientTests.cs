using System.Net.Http.Headers;
using NUnit.Framework;

namespace lagoclient.Tests;

[TestFixture]
public class LagoClientTests
{
    [Test, Explicit]
    public async Task CreateCustomer() {
        var httpClient = new HttpClient();
        httpClient.DefaultRequestHeaders.Authorization =
            new AuthenticationHeaderValue("Bearer", "6b1844ef-3efe-4932-88d6-a7b142be26ac");
        var sut = new LagoClient(httpClient);
        sut.BaseUrl = "https://lago.mybadges.io:3000/api/v1";
        
        var customerInput = new CustomerInput {
            Customer = {
                External_id = "abcdefg",
                Legal_name = "Test Customer",
                Billing_configuration = new BillingConfigurationCustomer {
                    Invoice_grace_period = 0,
                    Provider_customer_id = "111"
                }
            }
        };
        var customer = await sut.CreateCustomerAsync(customerInput);
    }
}