using NSwag;
using NSwag.CodeGeneration.CSharp;

namespace LagoClient.Tests;

public class Generator
{
    public async Task Generate() {
        var document =  await OpenApiYamlDocument.FromUrlAsync("http://swagger.getlago.com/swagger.yaml");
        
        var settings = new CSharpClientGeneratorSettings {
            ClassName = "LagoClient",
            CSharpGeneratorSettings = {
                Namespace = "lagoclient"
            }
        };

        var generator = new CSharpClientGenerator(document, settings);
        var code = generator.GenerateFile();
        File.WriteAllText("../../../../LagoClient/LagoClient.cs", code);
    }
}