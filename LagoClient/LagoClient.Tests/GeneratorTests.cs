using NUnit.Framework;

namespace LagoClient.Tests;

[TestFixture]
public class GeneratorTests
{
    [Test, Explicit]
    public async Task GenerateClient() {
        var sut = new Generator();
        await sut.Generate();
    }
}